package com.example2;


public class Calculation2 implements Runnable {
    private int num1;
    private int num2;
    private int sum;
    Thread threadObject;
    public Calculation2(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
        this.sum = 0;
        threadObject=new Thread(this,"additionThread");
        System.out.println("Thread started");
        threadObject.start();
    }
    public void run(){
        System.out.println("Thread in the Running state");
        this.sum=this.num1+this.num2;
        System.out.println(this.sum);
    }
}
