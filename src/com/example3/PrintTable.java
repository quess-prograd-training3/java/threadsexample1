package com.example3;

public class PrintTable extends Thread {
    private int number;
    Thread threadObject;

    public PrintTable(int number) {
        this.number = number;
    }
    public void run(){
        synchronized (this) {
            System.out.println("Thread is Running state");
            for (int iterate = 0; iterate <= 10; iterate++) {
                System.out.println(number + " x " + iterate + " = " + (number * iterate));
            }
        }
    }
}
