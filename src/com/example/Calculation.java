package com.example;

public class Calculation extends Thread {
    private int num1;
    private int num2;
    private int sum;

    public Calculation(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
        this.sum = 0;
    }
    public void run(){
        this.sum=this.num1+this.num2;
        System.out.println(this.sum);
    }

    public static void main(String[] args) {
        Calculation calculation=new Calculation(1,2);
        calculation.start();
    }
}
