package com.example4;

public class PrintTable2 implements Runnable {
    private int number;
    Thread threadObject;

    public PrintTable2(int number) {
        this.number = number;
        threadObject=new Thread(this,"Multiplication table");
        System.out.println("Thread started");
        threadObject.start();
    }
    public void run(){
        System.out.println("Thread is Running state");
        for (int iterate = 0; iterate <= 10; iterate++) {
            System.out.println(number + " x " + iterate + " = " + (number * iterate));
        }
    }
}
